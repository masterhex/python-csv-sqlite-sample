import csv
import sqlite3
import sys


filename = sys.argv[1]
con = sqlite3.connect("mydatabase.db") 
cur = con.cursor()

with open(filename) as f:
    for row in csv.reader(f):
        values = []
        for v in row:
            if isinstance (v, str):
                v = '"' + v + '"'
            values.append(v)
        values = ' ,'.join(values)

        stmt ='INSERT INTO myTable VALUES ({})'.format(values)
        print(stmt)
        cur.execute(stmt)

    con.commit()
con.close()
